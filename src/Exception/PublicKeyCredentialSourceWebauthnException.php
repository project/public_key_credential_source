<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Exception;

use Drupal\webauthn_framework\Exception\WebauthnFrameworkException;

/**
 * Webauthn exceptions for the Public Key Credential Source module.
 */
class PublicKeyCredentialSourceWebauthnException extends WebauthnFrameworkException {
}
