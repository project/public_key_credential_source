<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Exception;

/**
 * Exceptions for the Public Key Credential Source module.
 */
class PublicKeyCredentialSourceException extends \Exception {
}
