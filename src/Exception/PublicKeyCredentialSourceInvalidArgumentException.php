<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Exception;

/**
 * Invalid argument exception for the Public Key Credential Source module.
 */
class PublicKeyCredentialSourceInvalidArgumentException extends PublicKeyCredentialSourceException {
}
