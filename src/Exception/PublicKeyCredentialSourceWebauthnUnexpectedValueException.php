<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Exception;

/**
 * Webauthn unexpected value exceptions for the PKCS module.
 */
class PublicKeyCredentialSourceWebauthnUnexpectedValueException extends PublicKeyCredentialSourceWebauthnException {
}
