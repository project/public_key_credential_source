<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Exception;

/**
 * Webauthn validation failed exceptions for the PKCS module.
 */
class PublicKeyCredentialSourceWebauthnValidationFailedException extends PublicKeyCredentialSourceWebauthnException {
}
