<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Exception;

/**
 * Webauthn invalid argument exceptions for the PKCS module.
 */
class PublicKeyCredentialSourceWebauthnInvalidArgumentException extends PublicKeyCredentialSourceWebauthnException {
}
