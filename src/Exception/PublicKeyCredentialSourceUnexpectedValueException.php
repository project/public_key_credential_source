<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Exception;

/**
 * Unexpected value exception for the Public Key Credential Source module.
 */
class PublicKeyCredentialSourceUnexpectedValueException extends PublicKeyCredentialSourceException {
}
