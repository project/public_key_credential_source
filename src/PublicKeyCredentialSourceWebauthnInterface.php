<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source;

use Drupal\user\UserInterface;
use Webauthn\PublicKeyCredentialCreationOptions;
use Webauthn\PublicKeyCredentialSource as PKCredentialSource;

/**
 * Provides an interface for interacting with the Webauthn Framework.
 *
 * Based on the docs:
 * https://webauthn-doc.spomky-labs.com/pure-php/the-hard-way.
 */
interface PublicKeyCredentialSourceWebauthnInterface {

  /**
   * Gets the allowed credentials. Used for authentication and attestation.
   *
   * These are the excludeCredentials for registration,
   * and the valid credentials for authentication.
   *
   * @param string $user_email
   *   The email address of the user to get credentials for.
   *
   * @return array
   *   An array of Webauthn PublicKeyCredentialDescriptor objects.
   */
  public function getAllowedCredentials(string $user_email): array;

  /**
   * Get Server side Registration options.
   *
   * @param \Drupal\user\UserInterface $user
   *   The registration target user.
   *
   * @return \Webauthn\PublicKeyCredentialCreationOptions
   *   Server side registration options.
   */
  public function getRegistrationOptions(UserInterface $user): PublicKeyCredentialCreationOptions;

  /**
   * Gets the options for a user to authenticate (log in).
   *
   * @param string $user_email
   *   The email address of the user to authenticate.
   *
   * @return string
   *   The json stringified PublicKeyCredentialRequestOptions or error string.
   */
  public function getRequestOptions(string $user_email): string;

  /**
   * Registers a device to a user account.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to register a device for.
   * @param string $response
   *   The attestation response.
   */
  public function registerDevice(UserInterface $user, string $response): void;

  /**
   * Authenticates via passkey.
   *
   * You need to supply the user handle, which could be a Drupal uid,
   * email address, or uuid, depending on your site configuration.
   *
   * @param string $user_email
   *   The webauthn user handle. At present, only email address is supported.
   * @param string $request
   *   The passkey request.
   *
   * @returns PKCredentialSource|null
   *   The authenticated public key credential source.
   */
  public function authenticateRequest(string $user_email, string $request): ?PKCredentialSource;

}
