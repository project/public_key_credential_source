<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Hook;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hooks for the public_key_credential_source module.
 */
class PublicKeyCredentialSourceHooks {

  /**
   * Implements hook_jsonapi_ENTITY_TYPE_filter_access().
   *
   * Users who have created passkeys are permitted to filter among their own.
   * This facilitates viewing and deletion.
   */
  #[Hook('jsonapi_public_key_credential_source_filter_access')]
  public function filterAccess(EntityTypeInterface $entity_type, AccountInterface $account): array {
    return ([
      JSONAPI_FILTER_AMONG_ALL => AccessResult::allowedIfHasPermission($account, 'administer public key credential source entities'),
      JSONAPI_FILTER_AMONG_OWN => AccessResult::allowed(),
    ]);
  }

  /**
   * Implements hook_theme().
   */
  #[Hook('theme')]
  public function theme(): array {
    return [
      'public_key_credential_source' => ['render element' => 'elements'],
    ];
  }

}
