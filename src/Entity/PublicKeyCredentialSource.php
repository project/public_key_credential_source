<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\public_key_credential_source\Exception\PublicKeyCredentialSourceInvalidArgumentException;
use Drupal\public_key_credential_source\Exception\PublicKeyCredentialSourceUnexpectedValueException;
use Drupal\public_key_credential_source\PublicKeyCredentialSourceInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Webauthn\PublicKeyCredentialSource as PKCredentialSource;

/**
 * Defines the public key credential source entity class.
 *
 * @ContentEntityType(
 *   id = "public_key_credential_source",
 *   label = @Translation("Public Key Credential Source"),
 *   label_collection = @Translation("Public Key Credential Sources"),
 *   label_singular = @Translation("public key credential source"),
 *   label_plural = @Translation("public key credential sources"),
 *   label_count = @PluralTranslation(
 *     singular = "@count public key credential sources",
 *     plural = "@count public key credential sources",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\public_key_credential_source\PublicKeyCredentialSourceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\public_key_credential_source\PublicKeyCredentialSourceAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\public_key_credential_source\Form\PublicKeyCredentialSourceForm",
 *       "edit" = "Drupal\public_key_credential_source\Form\PublicKeyCredentialSourceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "public_key_credential_source",
 *   admin_permission = "administer public_key_credential_source",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uid" = "uid",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/public-key-credential-source",
 *     "add-form" = "/public-key-credential-source/add",
 *     "canonical" = "/public-key-credential-source/{public_key_credential_source}",
 *     "edit-form" = "/public-key-credential-source/{public_key_credential_source}/edit",
 *     "delete-form" = "/public-key-credential-source/{public_key_credential_source}/delete",
 *     "delete-multiple-form" = "/admin/content/public-key-credential-source/delete-multiple",
 *   },
 * )
 */
final class PublicKeyCredentialSource extends ContentEntityBase implements PublicKeyCredentialSourceInterface {
  /**
   * The source instance.
   *
   * @var \Webauthn\PublicKeyCredentialSource
   */
  private PKCredentialSource $sourceObject;

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function createFromSourceObject(PKCredentialSource $source): PublicKeyCredentialSourceInterface {
    if (\Drupal::currentUser()->isAnonymous()) {
      throw new PublicKeyCredentialSourceInvalidArgumentException('Cannot create public key credential for anonymous user!');
    }
    $uuid = \Drupal::service('uuid');
    /** @var \Drupal\webauthn_framework\WebauthnInterface $webauthn_framework */
    $webauthn_framework = \Drupal::service('webauthn_framework.webauthn');
    $entity_values = [
      'uuid' => $uuid->generate(),
      'uid' => \Drupal::currentUser()->id(),
      'created' => \Drupal::time()->getCurrentTime(),
      'public_key_credential_source' => $webauthn_framework->serializePublicKeyCredential($source),
    ];
    $entity = PublicKeyCredentialSource::create($entity_values);

    // Entity validation needs to be called manually when
    // creating entities programmatically.
    $violations = $entity->validate();
    if ($violations->count() > 0) {
      $error_message = 'Entity validation failed!';
      if ($violations[0] instanceof ConstraintViolationInterface) {
        $error_message .= " " . $violations[0]->getMessage();
      }
      throw new PublicKeyCredentialSourceInvalidArgumentException($error_message);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the public key credential source was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_used'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last used'))
      ->setDescription(t('The time that the public key credential source was last used.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created_device'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Created device'))
      ->setDescription(t('The user agent string for the device used to create the credential.'));

    $fields['last_used_device'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last used device'))
      ->setDescription(t('The user agent string for the device last used with the credential.'));

    $fields['public_key_credential_source'] = BaseFieldDefinition::create('json')
      ->setLabel(new TranslatableMarkup('Public Key Credential JSON'))
      ->setDescription(t('The public key credential JSON object.'))
      ->setRequired(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Owner'))
      ->setDescription(t('The user ID that owns this credential.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    return $fields;
  }

  /**
   * Returns the key for 'uid' to implement EntityOwnerInterface.
   *
   * @return string
   *   The owner key.
   */
  private function getOwnerKey(): string {
    $owner_key = $this->getEntityType()->getKey('uid');
    if ($owner_key === FALSE) {
      throw new PublicKeyCredentialSourceUnexpectedValueException('Could not get owner key!');
    }
    return $owner_key;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getOwnerId(): ?int {
    // Core currently may return int as a string.
    return intval($this->get($this->getOwnerKey())->target_id);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function setOwnerId($uid): PublicKeyCredentialSource|EntityOwnerInterface|static {
    $this->set($this->getOwnerKey(), $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function setOwner(UserInterface $account): PublicKeyCredentialSource|EntityOwnerInterface|static {
    $this->set($this->getOwnerKey(), $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getPublicKeyCredentialSource(): PKCredentialSource {
    if (!isset($this->sourceObject)) {
      $field_value = $this->public_key_credential_source->value;
      if (!is_string($field_value) || $field_value === '') {
        throw new PublicKeyCredentialSourceUnexpectedValueException('public_key_credential_source field has no value!');
      }
      $this->sourceObject = \Drupal::service('webauthn_framework.webauthn')->getPublicKeyCredentialSourceFromResponse($field_value);
    }

    return $this->sourceObject;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getPublicKeyCredentialId(): ?string {
    $public_key_credential_source = $this->getPublicKeyCredentialSource();
    return $public_key_credential_source->publicKeyCredentialId;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getDeviceId(): ?string {
    $public_key_credential_source = $this->getPublicKeyCredentialSource();
    return $public_key_credential_source->aaguid->jsonSerialize();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getCounter(): int {
    $public_key_credential_source = $this->getPublicKeyCredentialSource();
    return $public_key_credential_source->counter;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getOwner(): UserInterface {
    $owner = $this->get($this->getOwnerKey())->entity;
    /** @var \Drupal\user\UserInterface $owner */
    return $owner;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function preSave(EntityStorageInterface $storage): int {
    // Generating the id is computationally expensive.
    $current_device_id_string = $this->generateUserAgentString();

    if ($this->isNew()) {
      $this->set('created_device', $current_device_id_string);
    }

    $this->set('last_used_device', $current_device_id_string);
    $this->set('last_used', \Drupal::time()->getCurrentTime());

    parent::preSave($storage);
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function generateUserAgentString(): string {
    $detection_service = \Drupal::service('universal_device_detection.default');
    $detected_device = $detection_service->detect();
    $browser_name = $detected_device['info']['client']['name'];
    $os_name = $detected_device['info']['os']['name'];
    $os_version = $detected_device['info']['os']['version'];

    return "$os_name $os_version $browser_name";
  }

}
