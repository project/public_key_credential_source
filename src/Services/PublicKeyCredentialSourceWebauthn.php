<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source\Services;

use Cose\Algorithms;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\public_key_credential_source\Entity\PublicKeyCredentialSource;
use Drupal\public_key_credential_source\Exception\PublicKeyCredentialSourceWebauthnException;
use Drupal\public_key_credential_source\Exception\PublicKeyCredentialSourceWebauthnInvalidArgumentException;
use Drupal\public_key_credential_source\Exception\PublicKeyCredentialSourceWebauthnUnexpectedValueException;
use Drupal\public_key_credential_source\Exception\PublicKeyCredentialSourceWebauthnValidationFailedException;
use Drupal\public_key_credential_source\PublicKeyCredentialSourceInterface;
use Drupal\public_key_credential_source\PublicKeyCredentialSourceWebauthnInterface;
use Drupal\user\UserInterface;
use Drupal\webauthn_framework\WebauthnInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webauthn\AuthenticatorAssertionResponse;
use Webauthn\AuthenticatorAttestationResponse;
use Webauthn\AuthenticatorSelectionCriteria;
use Webauthn\PublicKeyCredential as PKCredential;
use Webauthn\PublicKeyCredentialCreationOptions;
use Webauthn\PublicKeyCredentialDescriptor;
use Webauthn\PublicKeyCredentialParameters;
use Webauthn\PublicKeyCredentialRequestOptions;
use Webauthn\PublicKeyCredentialSource as PKCredentialSource;

/**
 * Helper class to use webauthn-lib with public_key_credential_source module.
 */
final class PublicKeyCredentialSourceWebauthn implements ContainerInjectionInterface, PublicKeyCredentialSourceWebauthnInterface {
  use LoggerChannelTrait;

  /**
   * The private temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private PrivateTempStore $tempstore;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * The entity storage interface for public_key_credential_source entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $publicKeyCredentialSourceEntityStorage;

  /**
   * The key for storing PublicKeyCredentialCreationOptions.
   */
  private const string TEMP_STORE_CREATION_OPTIONS = 'credential_creation_options';

  /**
   * The key for storing PublicKeyCredentialRequestOptions.
   */
  private const string TEMP_STORE_REQUEST_OPTIONS = 'credential_request_options';

  /**
   * Webauthn helper constructor.
   */
  public function __construct(
    protected WebauthnInterface $webauthn,
    protected EntityTypeManagerInterface $entityTypeManager,
    PrivateTempStoreFactory $tempstore,
  ) {
    $this->tempstore = $tempstore->get('webauthn_framework');
    $this->logger = $this->getLogger('public_key_credential_source');
    $this->publicKeyCredentialSourceEntityStorage = $this->entityTypeManager->getStorage('public_key_credential_source');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): PublicKeyCredentialSourceWebauthnInterface {
    return new self(
      $container->get('webauthn_framework.webauthn'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
    );
  }

  /**
   * Gets all credentials for a user.
   *
   * @param int $uid
   *   The user id of the user to get credentials for.
   *
   * @return array
   *   An array of webauthn-lib PublicKeyCredentialSources, not Drupal entities!
   */
  private function getAllPublicKeyCredentialSources(int $uid): array {
    $query = $this->publicKeyCredentialSourceEntityStorage->getQuery()->accessCheck(TRUE);
    $results = $query->condition('uid', $uid)
      ->execute();

    // Stop processing if there are no results.
    if (!is_array($results)) {
      return [];
    }

    /** @var \Drupal\public_key_credential_source\PublicKeyCredentialSourceInterface[] $all_public_key_credential_source_entities */
    $all_public_key_credential_source_entities = $this->publicKeyCredentialSourceEntityStorage->loadMultiple($results);
    $all_webauthn_credential_sources = array_map(static fn(PublicKeyCredentialSourceInterface $entity): PKCredentialSource => $entity->getPublicKeyCredentialSource(), $all_public_key_credential_source_entities);

    return $all_webauthn_credential_sources;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getAllowedCredentials(string $user_email): array {
    $user = user_load_by_mail($user_email);
    if (!($user instanceof UserInterface)) {
      if ($this->webauthn->canDiscloseRegistration()) {
        throw new PublicKeyCredentialSourceWebauthnInvalidArgumentException("User email not associated with a Drupal account! $user_email");
      }
      else {
        return [];
      }
    }
    $uid = intval($user->id());
    $all_drupal_credentials = $this->getAllPublicKeyCredentialSources($uid);

    // We don’t need the Credential Sources, just the associated Descriptors.
    $allowed_credentials = array_map(
      static fn(PKCredentialSource $credential): PublicKeyCredentialDescriptor => $credential->getPublicKeyCredentialDescriptor(),
      $all_drupal_credentials
    );
    // This value will be converted to JSON,
    // so the array keys need to be sequential.
    $reindexed_allowed_credentials = array_values($allowed_credentials);
    return $reindexed_allowed_credentials;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getRegistrationOptions(UserInterface $user): PublicKeyCredentialCreationOptions {
    $user_entity = $this->webauthn->createUserEntity($user);

    $rp_entity = $this->webauthn->createRpEntity();

    $challenge = $this->webauthn->createChallenge();

    // Defaults to ES256 + RS256 by default.
    // https://stackoverflow.com/a/77468132
    $public_key_credential_parameters_list = [
      PublicKeyCredentialParameters::create('public-key', Algorithms::COSE_ALGORITHM_ES256),
      PublicKeyCredentialParameters::create('public-key', Algorithms::COSE_ALGORITHM_RS256),
    ];

    // Even though these are default values, they must be set here
    // in order to return them in the JSON response.
    $authenticator_selection_criteria = AuthenticatorSelectionCriteria::create(
      AuthenticatorSelectionCriteria::AUTHENTICATOR_ATTACHMENT_NO_PREFERENCE,
      AuthenticatorSelectionCriteria::USER_VERIFICATION_REQUIREMENT_PREFERRED,
      AuthenticatorSelectionCriteria::RESIDENT_KEY_REQUIREMENT_PREFERRED,
    );

    $user_email = $user->getEmail();
    if (!is_string($user_email)) {
      throw new PublicKeyCredentialSourceWebauthnUnexpectedValueException('Failed to get user uuid!');
    }
    $exclude_credentials = $this->getAllowedCredentials($user_email);

    $public_key_credential_creation_options = PublicKeyCredentialCreationOptions::create(
      $rp_entity,
      $user_entity,
      $challenge,
      $public_key_credential_parameters_list,
      $authenticator_selection_criteria,
      NULL,
      $exclude_credentials,
    );
    $this->tempstore->set($this::TEMP_STORE_CREATION_OPTIONS, $public_key_credential_creation_options);

    return $public_key_credential_creation_options;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getRequestOptions(string $user_email): string {
    try {
      $allowed_credentials = $this->getAllowedCredentials($user_email);
      $public_key_credential_request_options = PublicKeyCredentialRequestOptions::create(
        $this->webauthn->createChallenge(),
        allowCredentials: $allowed_credentials,
      );
      $this->tempstore->set(self::TEMP_STORE_REQUEST_OPTIONS, $public_key_credential_request_options);

      return $this->webauthn->serializePublicKeyCredential($public_key_credential_request_options);
    }
    catch (PublicKeyCredentialSourceWebauthnException $e) {
      if ($e instanceof PublicKeyCredentialSourceWebauthnInvalidArgumentException) {
        return $e->getMessage();
      }
      throw new PublicKeyCredentialSourceWebauthnException('Unexpected exception getting request!', 0, $e);
    }
  }

  /**
   * Creates a Drupal public_key_credential_source entity for the given input.
   *
   * @param \Webauthn\PublicKeyCredentialSource $public_key_credential_source
   *   The public key credential source sent via JSON:RPC.
   */
  private function createDrupalEntityPublicKeyCredentialSource(PKCredentialSource $public_key_credential_source): void {
    $public_key_credential_source_entity = PublicKeyCredentialSource::createFromSourceObject($public_key_credential_source);
    $storage = $this->entityTypeManager->getStorage('user');
    $user_array = $storage->loadByProperties(['uuid' => $public_key_credential_source->userHandle]);
    $user = reset($user_array);

    if (!($user instanceof UserInterface)) {
      $key = $public_key_credential_source->publicKeyCredentialId;
      $handle = $public_key_credential_source->userHandle;
      throw new PublicKeyCredentialSourceWebauthnInvalidArgumentException("Cannot save credential source $key, the user $handle does not exist.");
    }

    $public_key_credential_source_entity->save();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function registerDevice(UserInterface $user, string $response): void {
    $public_key_credential = $this->webauthn->getPublicKeyCredentialFromResponse($response);

    if ($public_key_credential->response instanceof AuthenticatorAttestationResponse) {
      /** @var \Webauthn\PublicKeyCredentialCreationOptions $saved_credentials_for_validation */
      $saved_credentials_for_validation = $this->tempstore->get(self::TEMP_STORE_CREATION_OPTIONS);

      $relying_party_id = $this->webauthn->getRpId();
      // This will throw an exception if something is wrong.
      $public_key_credential_source = $this->webauthn->authenticatorAttestationResponseValidator()->check(
        $public_key_credential->response,
        $saved_credentials_for_validation,
        $relying_party_id,
      );
      $uid = $user->id();
      $this->logger->debug("Valid public key credential returned for uid $uid!");
      $this->createDrupalEntityPublicKeyCredentialSource($public_key_credential_source);
    }
    else {
      throw new PublicKeyCredentialSourceWebauthnInvalidArgumentException('Not an AuthenticatorAttestationResponse!');
    }
  }

  /**
   * Finds a credential if it exists in the database.
   *
   * This used to be a method of PublicKeyCredentialSourceRepository,
   * findOneByCredentialId.
   *
   * @param \Webauthn\PublicKeyCredential $public_key_credential
   *   The public key credential ID to locate.
   *
   * @return \Drupal\public_key_credential_source\PublicKeyCredentialSourceInterface|null
   *   The public key credential source entity, if available.
   */
  private function getPublicKeyCredentialSourceFromId(PKCredential $public_key_credential): ?PublicKeyCredentialSourceInterface {
    $query = $this->publicKeyCredentialSourceEntityStorage->getQuery()->accessCheck(TRUE);
    $results = $query->condition('public_key_credential_source', $public_key_credential->rawId, 'CONTAINS')
      ->execute();

    // Stop processing if there are no results.
    if (!is_array($results)) {
      return NULL;
    }
    if (count($results) > 1) {
      throw new PublicKeyCredentialSourceWebauthnInvalidArgumentException('More than one public key credential source was returned!');
    }
    // Reindex the array so that the first value will be at the 0 position.
    $results = array_values($results);

    $entity = $this->publicKeyCredentialSourceEntityStorage->load($results[0]);
    /** @var \Drupal\public_key_credential_source\PublicKeyCredentialSourceInterface $entity */

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function authenticateRequest(string $user_email, string $request): ?PKCredentialSource {
    $public_key_credential = $this->webauthn->getPublicKeyCredentialFromResponse($request);
    $user = user_load_by_mail($user_email);
    if (!$user instanceof UserInterface) {
      // There is no user for the provided email address.
      return NULL;
    }
    // The userHandle for verification is the user uuid, which is used as the
    // webauthn user id when creating the key.
    $user_handle = $user->uuid();

    if ($public_key_credential->response instanceof AuthenticatorAssertionResponse) {
      /** @var \Webauthn\PublicKeyCredentialRequestOptions $saved_credentials_for_validation */
      $saved_credentials_for_validation = $this->tempstore->get(self::TEMP_STORE_REQUEST_OPTIONS);

      $public_key_credential_source_entity = $this->getPublicKeyCredentialSourceFromId($public_key_credential);

      if (!$public_key_credential_source_entity instanceof PublicKeyCredentialSourceInterface) {
        throw new PublicKeyCredentialSourceWebauthnUnexpectedValueException('Failed to get public key credential source entity!');
      }

      $public_key_credential_source_from_drupal_entity = $public_key_credential_source_entity->getPublicKeyCredentialSource();

      $relying_party_id = $this->webauthn->getRpId();
      try {
        $public_key_credential_source_webauthn = $this->webauthn->authenticatorAssertionResponseValidator()->check(
          $public_key_credential_source_from_drupal_entity,
          $public_key_credential->response,
          $saved_credentials_for_validation,
          $relying_party_id,
          $user_handle,
        );

        $this->logger->debug("Valid public key credential returned for user_handle $user_handle!");

        // Resave the Drupal entity to update the last device used info.
        $public_key_credential_source_entity->save();

        return $public_key_credential_source_webauthn;
      }
      catch (\Exception $e) {
        $exception_message = $e->getMessage();
        throw new PublicKeyCredentialSourceWebauthnValidationFailedException("Webauthn authentication failed! $exception_message", 0, $e);
      }
    }
    else {
      throw new PublicKeyCredentialSourceWebauthnInvalidArgumentException('Not an AuthenticatorAssertionResponse!');
    }
  }

}
