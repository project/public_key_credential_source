<?php

declare(strict_types=1);

namespace Drupal\public_key_credential_source;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for public key credential source entities.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class PublicKeyCredentialSourceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    /** @var \Drupal\public_key_credential_source\PublicKeyCredentialSourceInterface $entity */
    switch ($operation) {
      case 'view':
        $has_permission = $account->hasPermission('administer public key credential source entities');
        $is_authenticated = $account->isAuthenticated();
        // Core bug: https://www.drupal.org/project/drupal/issues/3224376
        $is_owner = intval($account->id()) === $entity->getOwnerId();

        return AccessResult::allowedIf(($is_authenticated && $is_owner) || $has_permission)
          ->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);

      case 'update':
        return AccessResult::forbidden('Public Key Credential Source entities cannot be edited.');

      case 'delete':
        $has_permission = $account->hasPermission('delete public key credential source entities');
        $is_authenticated = $account->isAuthenticated();
        $is_owner = $account->id() === $entity->getOwnerId();

        return AccessResult::allowedIf(($is_authenticated && $is_owner) || $has_permission)
          ->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items = NULL) {
    $no_access_fields = ['public_key_credential_source'];

    // The passkey itself is private info that should never be exposed,
    // even to admins.
    if (in_array($field_definition->getName(), $no_access_fields, TRUE)) {
      return AccessResult::forbidden();
    }

    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
