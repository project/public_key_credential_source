<?php

namespace Drupal\Tests\public_key_credential_source\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group public_key_credential_source
 */
class PublicKeyCredentialSourceInstallTest extends KernelTestBase {

  private const string MODULE_NAME = 'public_key_credential_source';
  private const array MODULE_FIELDS = [
    'id',
    'uuid',
    'last_used',
    'created_device',
    'last_used_device',
    'public_key_credential_source',
    'uid',
  ];

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);
    $entity_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions(self::MODULE_NAME, self::MODULE_NAME);
    foreach (self::MODULE_FIELDS as $field) {
      $this::assertTrue(isset($entity_fields[$field]));
    }

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
    $module_db_table = \Drupal::database()->schema()->tableExists(self::MODULE_NAME);
    $this::assertFalse($module_db_table);

    // Try installing and uninstalling again.
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);
    $entity_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions(self::MODULE_NAME, self::MODULE_NAME);
    foreach (self::MODULE_FIELDS as $field) {
      $this::assertTrue(isset($entity_fields[$field]));
    }

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
    $module_db_table = \Drupal::database()->schema()->tableExists(self::MODULE_NAME);
    $this::assertFalse($module_db_table);
  }

}
