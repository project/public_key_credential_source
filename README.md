# Public Key Credential Source module

## Overview

This module provides a public key credential source entity type for use in
WebAuthn flows.

## Dependencies

This module is based on [web-auth/webauthn-lib](https://github.com/web-auth/webauthn-lib).

The module stores the PublicKeyCredentialSource object as JSON using a
JSON field provided by the [JSON Field module](https://www.drupal.org/project/json_field).

The user agent string is parsed using the
[Universal Device Detection module](https://www.drupal.org/project/universal_device_detection).

## Related Modules

Check out the [Decoupled Passkeys module](https://www.drupal.org/project/decoupled_passkeys) for an example implementation of this module.

## Credits

This module started from the Public Key Credential Source entity in the [WebAuthn module](https://www.drupal.org/project/webauthn).
